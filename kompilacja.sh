#!/bin/bash

arm-none-eabi-gcc \
-mcpu=cortex-m3 \
-mthumb \
-Wall \
-ffunction-sections \
-g \
-O0 \
-c \
-DSTM32F103RB \
-DSTM32F10X_MD \
-DUSE_STDPERIPH_DRIVER \
-D__ASSEMBLY__ \
-I./sumolib \
-I./cmsis \
-I. \
-I./stm_lib \
-I./stm_lib/inc \
-I./cmsis_boot \
./sumolib/rprintf.c \
./stm_lib/src/stm32f10x_tim.c \
./sumolib/timer.c \
./sumolib/util.c \
./cmsis_boot/system_stm32f10x.c \
./sumolib/led.c \
./cmsis_boot/startup/startup_stm32f10x_md.c \
./stm_lib/src/stm32f10x_gpio.c \
./main.c \
./sumolib/sensor.c \
./stm_lib/src/stm32f10x_rcc.c \
./sumolib/motor.c \
./stm_lib/src/stm32f10x_adc.c \
./stm_lib/src/stm32f10x_usart.c \
./sumolib/servo.c \
./stm_lib/src/stm32f10x_dma.c \
./stm_lib/src/misc.c \
./sumolib/usart.c









