ifeq ($(OS),Windows_NT)
    EXE_SUFFIX = .exe
	RM := del /Q /F
	MKDIR := cmd /c if not exist "obj" mkdir "obj"
	PATHSEP := \\

else

    EXE_SUFFIX =.o
	RM := rm -rf
	MKDIR := mkdir -p obj
	PATHSEP := /
endif

SOURCES=$(wildcard sumolib/*.c)
SOURCES+=$(wildcard stm_lib/src/*.c)
SOURCES+=$(wildcard cmsis_boot/*.c)
SOURCES+=$(wildcard cmsis_boot/startup/*.c)
SOURCES+=$(wildcard stm_lib/*.c)


OBJECTS:=$(patsubst %.c, %.o, $(SOURCES))
OUTPUTS=$(wildcard obj/*.o)

ifeq ($(OS),Windows_NT)
	OBJECTS:=$(subst /,\,$(OBJECTS))
	OUTPUTS:=$(subst /,\,$(OUTPUTS))
endif

CC := arm-none-eabi-gcc
COMPINC := -I./sumolib -I./cmsis -I. -I./stm_lib -I./stm_lib/inc -I./cmsis_boot
CCOMPFLAGS = -mcpu=cortex-m3 -mthumb -Wall -ffunction-sections -g -O0 -c -DSTM32F103RB -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -D__ASSEMBLY__ $(COMPINC)
CLINKFLAGS = -mcpu=cortex-m3 -mthumb -g -nostartfiles -Wl,-Map=minisumo.map -O0 -Wl,--gc-sections -Wl,-T./link.ld


ifeq ($(OS),Windows_NT)
	CCOMPFLAGS:=$(subst /,\,$(CCOMPFLAGS))
	CLINKFLAGS:=$(subst /,\,$(CLINKFLAGS))
endif


all: build

before:
	$(MKDIR)

rebuild: clean compile

build: before compile main link hex

compile: $(OBJECTS)

$(OBJECTS): %.o : %.c
	$(CC) $(CCOMPFLAGS) -o obj$(PATHSEP)$(patsubst %.c,%.o,$(notdir $@)) $<

main:
	$(CC) $(CCOMPFLAGS) -o obj$(PATHSEP)main.o .$(PATHSEP)main.c

link:
	$(CC) $(CLINKFLAGS) -o minisumo.elf $(OUTPUTS)

hex:
	arm-none-eabi-objcopy -O ihex minisumo.elf minisumo.hex

clean:
	$(RM) obj

.PHONY: before rebuild compile clean